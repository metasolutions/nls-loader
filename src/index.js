const path = require('path');
const loaderUtils = require('loader-utils');


let nlsRootPath = path.join('src', 'nls');
let supportedLanguages = [];
let showWarnings = false;

const fs = require('fs');


const getNLSValue = (lang, nlsObject) => (lang === 'root' ? nlsObject.root : nlsObject);

const getFilePathsForLanguages = nlsComponent => [
  {
    lang: 'root',
    path: path.join(nlsRootPath, `${nlsComponent}.nls`),
  },
  ...supportedLanguages.map(lang => ({ lang, path: path.join(nlsRootPath, lang, `${nlsComponent}.nls`) })),
];

const loadNLSFile = (filePath) => {
  if (fs.existsSync(filePath)) {
    return JSON.parse(fs.readFileSync(filePath));
  }

  return {};
};

checkForUntranslatedStrings = (languageNLS, rootNLS) => {
  const nlsErrorStrings = [];
  Object.keys(rootNLS.nls).forEach((nlsStringName) => {
    if (languageNLS.nls[nlsStringName] == null || languageNLS.nls[nlsStringName].length <= 0) {
      nlsErrorStrings.push({
        lang: languageNLS.lang,
        nlsString: nlsStringName,
        path: languageNLS.path,
      });
    }
  });

  if (nlsErrorStrings.length > 0) {
    nlsErrorStrings.forEach(error => console.log(`${error.path}: ${error.nlsString} is missing a translation into ${error.lang}`));
  }
};

const loadNLSObjects = (filePaths) => {
  const nlsObjects = filePaths.map(langFileDef => (
    {
      lang: langFileDef.lang,
      nls: getNLSValue(langFileDef.lang, loadNLSFile(langFileDef.path)),
      path: langFileDef.path,
    }
  ));

  if (showWarnings) {
    const nlsPathErrors = [];

    nlsObjects.forEach((nlsObject) => {
      if (Object.keys(nlsObject.nls).length === 0) {
        if (nlsObject.lang !== 'nb') { // Ignore nb for now as it is not a required language but is included
          nlsPathErrors.push(`${nlsObject.path}:${nlsObject.lang}`);
        }
      } else {
        checkForUntranslatedStrings(nlsObject, nlsObjects.find(nls => nls.lang === 'root'));
      }
    });

    if (nlsPathErrors.length > 0) {
      process.emitWarning(`${nlsPathErrors} was not found but is supported in the configuration.`);
    }
  }

  return nlsObjects;
};

const getNLSObject = (nlsPath) => {
  const nlsPathAndComponent = nlsPath.split(path.sep);
  const nlsComponentName = nlsPathAndComponent[nlsPathAndComponent.length - 1].split('.nls').join('');

  const filePaths = getFilePathsForLanguages(nlsComponentName);
  const nlsObjects = loadNLSObjects(filePaths);

  return nlsObjects;
};

const loadNLS = function loadNLS() {
  const options = loaderUtils.getOptions(this);
  supportedLanguages = options.locales;
  showWarnings = options.showNLSWarnings == null ? showWarnings : options.showNLSWarnings;
  nlsRootPath = this.context;

  const nlsObject = getNLSObject(this.resourcePath);

  return `module.exports = ${JSON.stringify(nlsObject)};`;
};

module.exports = loadNLS;
